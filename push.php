<?php
header('SLC-Service: Git');
if (isset($_FILES['repository-zip'])) {
    $uploaded_zip = $_FILES['repository-zip'];
    if ($uploaded_zip['error'] === UPLOAD_ERR_OK and
        is_uploaded_file($uploaded_zip['tmp_name'])) {
        $zip_file = __DIR__.'/'.$uploaded_zip['name'];
        move_uploaded_file(
            $uploaded_zip['tmp_name'],
            $zip_file
        );
        $zip = new ZipArchive();
        $res = $zip->open($zip_file);

        if (TRUE === $res) {
            $zip->extractTo(__DIR__);
            $zip->close();
        }

        unlink($zip_file);
    }
} ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Git Push</title>
        <meta charset="UTF-8" />
    </head>
    <body>
        <p><code>upload_max_filesize</code>: <?php echo ini_get('upload_max_filesize'); ?></p>
        <p><code>max_file_uploads</code>: <?php echo ini_get('max_file_uploads'); ?></p>
        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST" enctype="multipart/form-data">
           <p><label for="repository-zip">Repository zip: <input type="file" name="repository-zip" id="repository-zip" /></label>
           <p><input type="submit" value="Upload" /></p>
        </form>
        <?php if (isset($_FILES['repository-zip'])) : ?>
        <p>Upload successful.</p>
        <?php endif; ?>
    </body>
</html>
