#!/bin/bash

# Activate checking for undefined variables
shopt -s -o nounset


# GLOBAL DECLARATIONS

declare -rx SCRIPT=${0##*/}		# This very script
declare -rx getopt="/usr/bin/getopt"	# Shortcut to getopt command
declare -rx git="/usr/bin/git"		# Shortcut to git command
declare -rx zip="/usr/bin/zip"		# Shortcut to zip command
declare -rx curl="/usr/bin/curl"		# Shortcut to curl command
declare -rx awk="/usr/bin/awk"		# Shortcut to awk command


# SANITY CHECKS

# If variable BASH's content is void (zero length), abort.
if [ -z $BASH ] ; then
  printf "$SCRIPT: please run this script with the BASH shell.\n" >&2
  exit 192
fi
# Check commands existence
if [ ! -x $getopt ] ; then
  printf "$SCRIPT: the command $getopt is not available.\n" >&2
  exit 192
fi
if [ ! -x $git ] ; then
  printf "$SCRIPT: the command $git is not available.\n" >&2
  exit 192
fi
if [ ! -x $zip ] ; then
  printf "$SCRIPT: the command $zip is not available.\n" >&2
  exit 192
fi
if [ ! -x $curl ] ; then
  printf "$SCRIPT: the command $curl is not available.\n" >&2
  exit 192
fi
if [ ! -x $awk ] ; then
  printf "$SCRIPT: the command $awk is not available.\n" >&2
  exit 192
fi


# FUNCTIONS

# alert-continue: displays a message informing the user of an exceptional
#		condition, and asking him if he wants to continue anyway.
# Argument $1: the message to display
# Argument $2: the variable where to store the return value
# Return: true (command) meaning to continue, or false (command)
#		meaning to stop
function alert-continue {
  local __RETURN_VAR=$2
  
  # Default return: true, meaning to continue
  local __LOCAL_RETURN=true
  
  local VALID_ANSWER=false
  while ! $VALID_ANSWER ; do
    printf "$1. Do you wish to continue anyway? (y|n) "
    read CONTINUE
    
    # It's always a valid anwser except for when it's different
    # from 'y' or 'n'
    VALID_ANSWER=true
    
    # The user wants to exit: return false to the caller
    if [ "$CONTINUE" == 'n' ] ; then
      __LOCAL_RETURN=false
    
    # Invalid answer, retry
    elif [ "$CONTINUE" != 'y' ] ; then
      printf "Please answer with either 'y' (yes) or 'n' (no).\n"
      VALID_ANSWER=false
    fi
  done
  
  # Fills the user-provided variable with the just-computed return value
  eval $__RETURN_VAR='$__LOCAL_RETURN'
}


# PARSE COMMAND LINE ARGUMENTS

# Options flags
FLAG_HELP=false
FLAG_VERBOSE=false
FLAG_DIRECTORY=false
DIRECTORY_VALUE=''
FLAG_REMOTE=false
REMOTE_VALUE=''
FLAG_WEBSERVICE=false
WEBSERVICE_VALUE=''
# Flags that an option has been used. Useful for those options
# that cannot be used with any other option, like -h
FLAG_USED_OPTION=false
# Flags that a unique-use option has been used
FLAG_UNIQUE_OPTION=false
# When the user tries to use multiple exclusive flags,
# throw an error with this message
ERROR_EXCLUSIVE_OPTIONS=''

# Use getopt to fetch arguments
PARSED_OPTIONS=$(getopt -n "$0" -o "hvd:r:w:" -l "help,verbose,directory:,remote:,web-service:" -- "$@")
# When getopt fails, it displays an error message, but does not exit
# with an error status.
if [ $? -ne 0 ] ; then
  exit 192
fi
# Set command line options as environment variables
eval set -- "$PARSED_OPTIONS"

# Set flags according to the command line options used
while true ; do
  case "$1" in
    -h|--help)
      # This flag should only be used alone
      if ! $FLAG_USED_OPTION ; then
	FLAG_HELP=true
	FLAG_USED_OPTION=true
	FLAG_UNIQUE_OPTION=true
      else
	ERROR_EXCLUSIVE_OPTIONS="Option -h|--help cannot be used with any other option."
	break
      fi
      shift;;
    -v|--verbose)
      # Cannot be used with an unique option
      if ! $FLAG_UNIQUE_OPTION ; then
	FLAG_VERBOSE=true
	FLAG_USED_OPTION=true
      else
	ERROR_EXCLUSIVE_OPTIONS="Option -v|--verbose cannot be used with option -h|--help."
	break
      fi
      shift;;
    -d|--directory)
      # Cannot be used with an unique option
      if ! $FLAG_UNIQUE_OPTION ; then
	FLAG_DIRECTORY=true
	FLAG_USED_OPTION=true
	DIRECTORY_VALUE=$2
      else
	ERROR_EXCLUSIVE_OPTIONS="Option -d|--directory cannot be used with option -h|--help."
	break
      fi
      shift 2;;
    -r|--remote)
      # Cannot be used with an unique option
      if ! $FLAG_UNIQUE_OPTION ; then
	FLAG_REMOTE=true
	FLAG_USED_OPTION=true
	REMOTE_VALUE=$2
      else
	ERROR_EXCLUSIVE_OPTIONS="Option -r|--remote cannot be used with option -h|--help."
	break;
      fi
      shift 2;;
    -w|--web-service)
      # Cannot be used with an unique option
      if ! $FLAG_UNIQUE_OPTION ; then
	FLAG_WEBSERVICE=true
	FLAG_USED_OPTION=true
	WEBSERVICE_VALUE=$2
      else
	ERROR_EXCLUSIVE_OPTIONS="Option -w|--web-service cannot be used with option -h|--help."
	break;
      fi
      shift 2;;
    --)
      shift
      break;;
  esac
done

# Exit on error using options
if [ -n "$ERROR_EXCLUSIVE_OPTIONS" ] ; then
  printf "$SCRIPT: $ERROR_EXCLUSIVE_OPTIONS\n" >&2
  exit 192
fi

# Print help if help option
if $FLAG_HELP ; then
  printf "usage: git-hard-push [-h|--help]
		     [-v|--verbose]
		     [-d<dir>|-d <dir>|--directory <dir>|--directory=<dir>]
		     [-r<remote>|-r <remote>|--remote <remote> | --remote=<remote>]
		     [-w<webservice>|-w <webservice>|--web-service <webservice>|--web-service=<web-service>]\n
Options:
    -h|--help		Print this help menu,
    -v|--verbose	Show additional information during execution.
    -d|--directory	Use <dir> as Git repository. Default is current working directory.
    -r|--remote		Use <remote> as remote Web service URL or remote Git name. Default is origin.
    -w|--web-service	Use <webservice> as relative Web path to the Web service. Default is /git/push/\n"
fi


# MANUALLY PUSH A GIT HTTP REPOSITORY ONTO A SERVER
# WHERE GIT IS NOT INSTALLED

# If option '-d|--directory' was used, change to that directory.
# Otherwise, use current working directory
if $FLAG_DIRECTORY ; then
  if [ -d $DIRECTORY_VALUE ] ; then
    cd $DIRECTORY_VALUE
    $FLAG_VERBOSE && printf "Changed to directory $DIRECTORY_VALUE.\n"
  else
    printf "$SCRIPT: $DIRECTORY_VALUE is not a valid directory name.\n" >&2
    exit 192
  fi
fi

# Check if this is a valid Git repository
$FLAG_VERBOSE && printf "Checking if directory `pwd` is a valid Git repository...\n"
GIT_REV_PARSE_RESULT=$($git rev-parse --git-dir 2> /dev/null)
if [ "$GIT_REV_PARSE_RESULT" != '.git' ] ; then
  printf "$SCRIPT: Directory `pwd` is not a valid Git repository.\n" >&2
  exit 192
fi
$FLAG_VERBOSE && printf "done.\n"

# Check if user specified a remote name
$FLAG_VERBOSE && printf "Checking remote repository...\n"
GIT_REMOTE_URL=''
if $FLAG_REMOTE ; then
  GIT_REMOTE_URL=$($git config --get remote.$REMOTE_VALUE.url)
  if [ -z "$GIT_REMOTE_URL" ] ; then
    # Not a Git remote. Check if it's a real URL
    GIT_REMOTE_HTML_CODE=$($curl -s -o /dev/null -w "%{http_code}" $REMOTE_VALUE/)
    if [ $GIT_REMOTE_HTML_CODE -eq 403 ] ; then
      GIT_REMOTE_URL=$REMOTE_VALUE
    else
      printf "$SCRIPT: $REMOTE_VALUE is not a valid Git remote repository.\n" >&2
      exit 192
    fi
  fi
else
  # No remote specified. Check for Git origin
  GIT_REMOTE_URL=$($git config --get remote.origin.url)
  if [ -z "$GIT_REMOTE_URL" ] ; then
    printf "$SCRIPT: No remote specified, and no Git remote origin found.\n" >&2
    exit 192
  fi
fi
$FLAG_VERBOSE && printf "done.\nRemote repository: $GIT_REMOTE_URL.\n"

# Extract remote host
$FLAG_VERBOSE && printf "Extracting remote host...\n"
GIT_HOST=http://$(echo $GIT_REMOTE_URL | awk -F/ '{print $3}')
$FLAG_VERBOSE && printf "done. Remote host is $GIT_HOST.\n"

# Build Web service address
$FLAG_VERBOSE && printf "Checking Web service...\n"
WEBSERVICE_ADDRESS=''
if $FLAG_WEBSERVICE ; then
  WEBSERVICE_ADDRESS=$GIT_HOST$WEBSERVICE_VALUE
else
  WEBSERVICE_ADDRESS=$GIT_HOST/git/push/
fi
WEBSERVICE_HTML_CODE=$($curl -s -o /dev/null -w "%{http_code}" $WEBSERVICE_ADDRESS/)
if [ $WEBSERVICE_HTML_CODE -ne 200 ] ; then
  printf "$SCRIPT: Web service $WEBSERVICE_ADDRESS is not a valid Web address.\n" >&2
  exit 192
fi
$FLAG_VERBOSE && printf "done. Web service address is $WEBSERVICE_ADDRESS.\n"

# Cloning a Git repository doesn't throw errors. If there are uncommitted or
# unstaged changes, they simply won't appear inside the clone.

# If there are untracked files, warn the user...
$FLAG_VERBOSE && printf "Checking untracked files...\n"
GIT_UNTRACKED_FILES=$($git ls-files --other --exclude-standard --directory)
if [ ! -z "$GIT_UNTRACKED_FILES" ] ; then
  alert-continue "This directory has untracked files" GIT_UNTRACKED_CONTINUE
  $GIT_UNTRACKED_CONTINUE || exit 0
else
  $FLAG_VERBOSE && printf "done.\n"
fi

# If there are uncommitted files, warn the user...
$FLAG_VERBOSE && printf "Checking uncommitted files...\n"
GIT_UNCOMMITTED_FILES=$($git status --porcelain)
if [ ! -z "$GIT_UNCOMMITTED_FILES" ] ; then
  alert-continue "This directory has uncommitted files" GIT_UNCOMMITTED_CONTINUE
  $GIT_UNCOMMITTED_CONTINUE || exit 0
else
  $FLAG_VERBOSE && printf "done.\n"
fi

GIT_REPO=`pwd`
GIT_REPO_NAME=$(basename `git rev-parse --show-toplevel`)
GIT_TMP_REPO=~/.slc/git-hard-push/$GIT_REPO_NAME
GIT_TMP=$(dirname $GIT_TMP_REPO)

# Bare clone to temp directory (~/.slc/git-hard-push/)
$FLAG_VERBOSE && printf "Cloning repository $GIT_REPO to temporary directory $GIT_TMP_REPO...\n"
git clone --bare . $GIT_TMP_REPO > /dev/null
$FLAG_VERBOSE && printf "done.\n"

# Run git update-server-info on temp directory
$FLAG_VERBOSE && printf "Running update-server-info on directory $GIT_TMP_REPO...\n"
cd $GIT_TMP_REPO
git update-server-info
$FLAG_VERBOSE && printf "done.\n"

# Zip tmp repo
GIT_ZIP=$GIT_REPO_NAME.zip
GIT_ZIP_PATH=$GIT_TMP/$GIT_ZIP
$FLAG_VERBOSE && printf "Zipping temporary repository $GIT_TMP_REPO to $GIT_ZIP_PATH...\n"
cd $GIT_TMP
zip -r $GIT_ZIP $GIT_REPO_NAME &> /dev/null
$FLAG_VERBOSE && printf "done.\n"

# PUSH zip file to Web Service
$FLAG_VERBOSE && printf "Pushing file $GIT_ZIP_PATH to Web service $WEBSERVICE_ADDRESS...\n"
curl -F "repository-zip=@$GIT_ZIP" $WEBSERVICE_ADDRESS &> /dev/null
$FLAG_VERBOSE && printf "done.\n"

# Cleanup temporary files
$FLAG_VERBOSE && printf "Removing temporary repository $GIT_TMP_REPO...\n"
rm -rf $GIT_TMP_REPO
$FLAG_VERBOSE && printf "done.\n"

GIT_TMP_ZIP_PATH=$GIT_TMP/$GIT_REPO_NAME.zip
$FLAG_VERBOSE && printf "Removing temporary zip file $GIT_TMP_ZIP_PATH...\n"
rm $GIT_TMP_ZIP_PATH
$FLAG_VERBOSE && printf "done.\n"